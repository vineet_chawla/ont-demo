// reads in our .env file and makes those values available as environment variables
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const passport = require('passport');
const path = require('path');
const hbs = require('express-handlebars');
const redis = require('redis');
const session = require('express-session');
const client = redis.createClient();
const redisStore = require('connect-redis')(session);

require('dotenv').config();
require('babel-polyfill');
const routes = require('./dist/routes/main');
const secureRoutes = require('./dist/routes/secure');
const passwordRoutes = require('./dist/routes/password');

// setup mongo connection
const uri = process.env.MONGODB_URI || 'mongodb://localhost/ont-demo';
mongoose.connect(uri, {useNewUrlParser: true, useCreateIndex: true});
mongoose.connection.on('error', (error) => {
  console.log(error);
  process.exit(1);
});
mongoose.connection.on('connected', function() {
  console.log('connected to mongo');
});
mongoose.set('useFindAndModify', false);

// create an instance of an express app
const app = express();

app.engine('hbs', hbs({
  extname: 'hbs',
  // defaultLayout: 'layout',
  // layoutsDir: __dirname + '/public/views/layouts/',
  // partialsDir: __dirname + 'public/views/partials'
}));
app.set('views', path.join(__dirname, '/public/views'));
app.set('view engine', 'hbs');


// update express settings
app.use(bodyParser.urlencoded({extended: false})); // parse application/x-www-form-urlencoded
app.use(bodyParser.json()); // parse application/json
app.use(session({
  secret: 'test',
  store: new redisStore({host: 'localhost', port: 6379, client: client, ttl: 260}),
  saveUninitialized: false,
  resave: false
}));
app.use(cookieParser());

// require passport auth
require('./dist/auth/auth');

app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res) {
  sess = req.session;
  if (sess.username) {
    return res.redirect('/test');
  }
  res.sendFile(__dirname + '/index.html');
});

// main routes
app.use('/', routes);
app.use('/', passwordRoutes);
app.use('/', passport.authenticate('jwt', {session: false}), secureRoutes);

// catch all other routes
app.use((req, res, next) => {
  res.status(404).json({message: '404 - Not Found'});
});

// handle errors
app.use((err, req, res, next) => {
  console.log(err.message);
  res.status(err.status || 500).json({error: err.message});
});

// have the server start listening on the provided port
app.listen(process.env.PORT || 3000, () => {
  console.log(`Server started on port ${process.env.PORT || 3000}`);
});

const Ont = require('ontology-ts-sdk');
// const API = require('ontology-dapi');
const WalletModel = require('../models/walletModel');
const client = require('ontology-dapi').client;
try {
  client.registerClient({});
} catch (err) {
  console.log(err);
}

const gasLimit = '20000';
const gasPrice = '500';

const parseBigInt = (val) => {
  return new BigNumber.BigNumber(API.utils.reverseHex(val), 16).toString();
};

// using default keys specifications
// should be same for decryption & encryption

/**
 * @description creates a wallet and user account with same name
 * @param {string} password user's password to encrypt priv key
 * @param {string} label name of wallet, by default username
 */
const createAccount = async (password, label) => {
  const wallet = Ont.Wallet.create(label);
  const privateKey = createPrivateKey();
  const account = Ont.Account.create(privateKey, password, label);
  wallet.addAccount(account);
  wallet.defaultAccountAddress = wallet.accounts[0].address.value;

  // needs better schema
  await WalletModel.create(wallet);
  // let x = await WalletModel.getAddress('vineetchawla');
  // console.log(x);
  // await AccountModel.create(account);
};

/**
 * @return {Object} privateKey object
 */
const createPrivateKey = () => {
  const keyType = Ont.Crypto.KeyType.ECDSA;
  const keyParameters = new Ont.Crypto.KeyParameters(Ont.Crypto.CurveLabel.SECP256R1);
  const privateKey = Ont.Crypto.PrivateKey.random(keyType, keyParameters);
  return privateKey;
};

const getBalance = async (contractHash) => {
  const method = 'balanceOf';
  const parameters = [];
  const params = {
    contract,
    method,
    parameters,
    gasPrice,
    gasLimit
  };
  const result = await scInvoke(params);
  if (result) {
    const val = parseBigInt(result);
    // add console statements
  }
};

const network = client.api.network.getNetwork();

const scInvoke = async (contract, params, preExec) => {
  if (contract === '') {
    return console.error('The contract has not been deployed yet!');
  };
  try {
    const result = await client.api.smartContract.invoke(params);
    console.log('onScCall finished, result: ' + JSON.stringify(result));
    return result;
  } catch (e) {
    console.log('onScCall error:', e);
    console.error('Error while invoking smart contract function');
    return null;
  }
};

module.exports = {
  createPrivateKey,
  createAccount,
  scInvoke
};

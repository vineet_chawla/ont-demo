const passport = require('passport');
const express = require('express');
const jwt = require('jsonwebtoken');
const path = require('path');
const Wallet = require('../models/walletModel');

const tokenList = {};
const router = express.Router();
let sess;

router.get('/status', (req, res, next) => {
  res.status(200).json({status: 'ok'});
});

router.post('/signup', passport.authenticate('signup', {session: false}), async (req, res, next) => {
  res.status(200).json({message: 'signup successful'});
});

router.post('/login', async (req, res, next) => {
  passport.authenticate('login', async (err, user, info) => {
    try {
      if (err || !user) {
        console.log(err);
        const error = new Error('An Error occured');
        return next(error);
      }
      req.login(user, {session: false}, async (error) => {
        if (error) return next(error);
        const body = {
          _id: user._id,
          email: user.email
        };

        const token = jwt.sign({user: body}, 'top_secret', {expiresIn: 300});
        const refreshToken = jwt.sign({user: body}, 'top_secret_refresh', {expiresIn: 86400});

        // store tokens in cookie
        res.cookie('jwt', token);
        res.cookie('refreshJwt', refreshToken);

        sess = req.session;
        sess.email = user.email;
        // store tokens in memory
        tokenList[refreshToken] = {
          token,
          refreshToken,
          email: user.email,
          _id: user._id
        };

        // Send back the token to the user
        return res.status(200).json({token, refreshToken});
      });
    } catch (error) {
      return next(error);
    }
  })(req, res, next);
});

router.post('/token', (req, res) => {
  const {refreshToken} = req.body;
  if (refreshToken in tokenList) {
    const body = {email: tokenList[refreshToken].email, _id: tokenList[refreshToken]._id};
    const token = jwt.sign({user: body}, 'top_secret', {expiresIn: 300});

    // update jwt
    res.cookie('jwt', token);
    tokenList[refreshToken].token = token;

    res.status(200).json({token});
  } else {
    res.status(401).json({message: 'Unauthorized'});
  }
});

router.post('/getAddress', async (req, res) => {
  const {email} = req.body;
  const wallet = await Wallet.findOne({name: email});
  res.send(wallet.defaultAccountAddress);
});

router.get('/allAddresses', async (req, res) => {
  const addresses = await Wallet.find({}, {_id: 0, defaultAccountAddress: 1});
  res.send(addresses);
});

// This code here assumes each wallet has one account.
router.get('/test', async (req, res, next) => {
  const addresses = await Wallet.find({}, {_id: 0, defaultAccountAddress: 1});
  const ownAddress = await Wallet.find({name: req.session.email}, {_id: 0, defaultAccountAddress: 1});
  const addressArray = [];
  addresses.map((x) => {
    // skip our own address
    if ( ownAddress[0].defaultAccountAddress == x['defaultAccountAddress']) {
      // skipping own address
    } else {
      addressArray.push(x['defaultAccountAddress']);
    }
  });
  // addresses = Object.values(addresses[0]);
  res.render('index', {layout: 'default', addresses: addressArray, ownAddress: ownAddress[0].defaultAccountAddress});
});

router.post('/logout', (req, res) => {
  if (req.cookies) {
    const refreshToken = req.cookies['refreshJwt'];
    if (refreshToken in tokenList) delete tokenList[refreshToken];
    res.clearCookie('refreshJwt');
    res.clearCookie('jwt');
  }
  req.session.destroy((err) => {
    if (err) {
      return console.log(err);
    }
    res.redirect('/');
  });
  return res.sendFile(path.join(__dirname + './../public/index.html'));
  // res.status(200).json({ message: 'logged out' });
});

const queryBalance = () => {

};

module.exports = router;

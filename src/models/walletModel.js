const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const WalletSchema = new Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  defaultOntid: {
    type: String
  },
  defaultAccountAddress: {
    type: String
  },
  createTime: {
    type: Date
  },
  version: {
    type: String
  },
  scrypt: {
    n: {
      type: String
    },
    r: {
      type: String
    },
    p: {
      type: String
    },
    dkLen: {
      type: String
    }
  },
  identities: {
    type: Array
  },
  accounts: {
    type: Array
  },
  extra: {
    type: String
  }
});

const WalletModel = mongoose.model('cryptoWallet', WalletSchema);

module.exports = WalletModel;
